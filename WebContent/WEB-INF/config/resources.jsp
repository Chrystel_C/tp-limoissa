<!-- Custom CSS -->
<c:url value='/resources/css/main.css' var="customStyleCSS"/>

<!-- Vendors -->
<c:url value='/resources/lib/bootstrap/dist/css/bootstrap.min.css' var="BootstrapCSS"/>
<c:url value='/resources/lib/bootstrap/dist/js/bootstrap.min.js' var="BootstrapJS"/>
<c:url value='/resources/lib/jquery/dist/jquery.min.js' var="jQueryJS"/>

<!-- Assets -->
<c:url value='/resources/assets/logo.png' var="logo"/>