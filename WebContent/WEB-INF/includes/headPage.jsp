<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link type="text/css" rel="stylesheet" href="${BootstrapCSS}"/>
		<link type="text/css" rel="stylesheet" href="${customStyleCSS}"/>
		<title>${title}</title>
	</head>
	<body>
	<%@ include file="navbar.jsp" %>
	<div class="container-fluid">