		</div>
		<div class="container-fluid">
			<footer class="text-center">
				<p>&copy; <fmt:formatDate pattern="YYYY" value="<%= new java.util.Date() %>"/> Chrystel Chagny</p>
				<a href="#"><img src="${logo}" alt="Logo" id="logo"/></a>
			</footer>
		</div>
		<script type="text/javascript" src="${jQueryJS}"></script>
		<script type="text/javascript" src="${BootstrapJS}"></script>
	</body>
</html>