package chrys.business;

import javax.servlet.http.HttpServletRequest;

import chrys.web.util.AbstractAction;
import chrys.web.util.Constants;

public class ContactAction extends AbstractAction {
	
	private static final String JSP_PAGE = Constants.JSP_CONTACT_NAME;
	private static final String TITLE = Constants.TITLE_CONTACT;

	@Override
	public String executeAction(HttpServletRequest request) {
		request.setAttribute("title", TITLE);
		return JSP_PAGE;
	}

}
